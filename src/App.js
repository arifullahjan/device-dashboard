import React from 'react';

function App() {
    return (
        <div className='instructions'>
            <h1>Relayr Device Dashboard</h1>
            <p>Feel free to implement UI the way you like.</p>
        </div>
    );
}

export default App;
